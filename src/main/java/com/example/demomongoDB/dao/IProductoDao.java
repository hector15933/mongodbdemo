package com.example.demomongoDB.dao;

import com.example.demomongoDB.document.Producto;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface IProductoDao extends ReactiveMongoRepository<Producto,String> {
}
