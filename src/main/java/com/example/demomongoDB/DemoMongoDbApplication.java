package com.example.demomongoDB;

import com.example.demomongoDB.dao.IProductoDao;
import com.example.demomongoDB.document.Producto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import reactor.core.publisher.Flux;

import java.util.Date;

@SpringBootApplication
public class DemoMongoDbApplication implements CommandLineRunner {


	@Autowired
	private IProductoDao productoDao;

	@Autowired
	private ReactiveMongoTemplate mongoTemplate;

	private static final Logger log = LoggerFactory.getLogger(DemoMongoDbApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DemoMongoDbApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		mongoTemplate.dropCollection("producto").subscribe();

		Flux.just(new Producto("Panasonic",456.89),
					new Producto("Sony",177.89),
					new Producto("Apple Ipad",456.89)
		)
				.flatMap(producto -> {
					producto.setCreateAt(new Date());
					return productoDao.save(producto);
				})
				.subscribe();
	}
}
